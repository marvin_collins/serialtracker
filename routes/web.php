<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Auth::routes();
Route::group(['middleware' => 'setting'], function () {
    Route::get('/', 'HomeController@index');
    Route::resource('grv', 'GetGRVController');
    Route::resource('issue', 'IssueController');
    Route::resource('serialized', 'SerializedItemsController');
    Route::resource('serials', 'SerialsearchController');
    Route::resource('warranties', 'ItemWarrantController');
    Route::resource('warrantysetup', 'WarrantyController');
    Route::resource('issueddnote', 'IssuedDnoteController');
    Route::get('issuednote', 'IssueDnoteController@issue');
    Route::post('receive', 'GetGRVController@receive');

    Route::get('/home', 'HomeController@index');
});
Route::resource('setting', 'SettingController');
