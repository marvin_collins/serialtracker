<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Issue extends Model
{
    const NOT_ISSUED = 'Not issued';
    const PARTIALLY_ISSUED = 'Partially issued';
    const FULLY_ISSUED = 'Fully issued';
    const DELIVERED = 'Fully Delivered';
    const PARTIALLY_DELIVERED = 'Partially Delivered';
    protected $dateFormat = 'Y-m-d H:i:s';
    protected  $fillable = ['sagegrv_id','autoindex_id','InvNumber','GrvNumber','Description','DeliveryDate','clientAccount','status'];
}
