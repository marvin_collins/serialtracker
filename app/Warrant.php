<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warrant extends Model
{
    protected $fillable = ['wrty_duration'];
    protected $dateFormat = 'Y-m-d H:i:s';
}
