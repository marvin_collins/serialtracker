<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GrvSerialized extends Model
{
    protected $fillable = ['autoindex_id','grvlines_id'];
    protected $dateFormat = 'Y-m-d H:i:s';
}



