<?php

namespace App\Http\Controllers;

use App\DnoteNumber;
use App\Issue;
use App\Issuelines;
use App\Sageitemsserial;
use App\Setting;
use App\Dnote;
use Illuminate\Http\Request;

use App\Http\Requests;
use Serial\ActivateSerial;
use Serial\GetSageDate;
use Serial\Helper;
use Serial\IssueInlines;
use Serial\IssueSo;
use Serial\AllGrv;
use Serial\Serializing;

class IssueController extends Controller
{

    public function __construct()
    {
        $this->middleware('setting');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('issue.index')->with('invoice',Issue::all()->sortBy('InvNumber'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource iserialno_2n storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $serialcolm = Helper::helper()->getPrimarySerialColn();
        $allDetails = Sageitemsserial::where($serialcolm,$request->get('serial'))->first();
        if(empty($allDetails)){
            return 'Serial not found';
        }
        elseif ($allDetails->status != Sageitemsserial::VALID_SERIAL)
        {
            return $allDetails->status;
        }

        elseif ($allDetails->status == null)
        {
            return 'Serial not found';
        }

        elseif ($request->get('code') != $allDetails->code)
        {
            return 'Item code does not match serial code';
        }

        else {
            Serializing::Serializing()->updateSerial($allDetails,$request->all());
            return $allDetails;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($autoindex)
    {
        $details = Issue::where('autoindex_id',$autoindex)->first();
        $inlinesdetails = Issuelines::where('autoindex_id',$autoindex)->get();
        return view('issue.show')
            ->with('inlines',$inlinesdetails)
            ->withDetails($details);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($autoindex)
    {
//        IssueInlines::issueinlines()->updateIssues($autoindex);
        $details = Issue::where('autoindex_id',$autoindex)->first();
        $inlinesdetails = Issuelines::where('autoindex_id',$autoindex)->get();
//        dd($details,$inlinesdetails);
        return view('issue.edit')
            ->with('inlines',$inlinesdetails)
            ->withDetails($details)
            ->withDnote(Helper::helper()->getDnoteNumber());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
