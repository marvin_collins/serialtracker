@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-7">
                    <div class="panel panel-info">
                        <div class="panel-heading mainThemeColor">
                            <a href="{{route('issue.index')}}" class="btn pull-left btn-sm btn-default btn-raised">
                                Back
                            </a>
                            <h3  align="center">Sale Order Invoice Details</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6 text-left">
                                    <h5 >INV NO: {{$details->InvNumber}}</h5>
                                    <h5 >Description: {{ucwords($details->Description)}}</h5>

                                </div>
                                <div class="col-md-6">
                                    <h5 class="text-right">Client Name: {{ucwords($details->clientAccount)}}</h5>
                                    <h5 class="text-right">Status: {{$details->status}}</h5>
                                    <h5 class="text-right">Date: {{\Carbon\Carbon::parse($details->created_at)->format(\Serial\Helper::helper()->dateFormat())}}</h5>
                                </div>
                            </div>
                            <hr />
                            <div class="col-md-12">
                                <table id="example" class="table table-striped table-hover table-responsive">
                                    <thead>
                                    <tr>
                                        <th>NO :</th>
                                        <th>Item Code</th>
                                        <th>Description</th>
                                        <th class="text-center">Ordered Qty</th>
                                        <th class="text-right">Remaining Qty</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($inlines as $invline)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$invline->code}}</td>
                                            <td>{{ ucwords($invline->cDescription)}}</td>
                                            <td class="text-right globalmargin" id="total_{{$invline->idInvoiceLines}}">{{($invline->previous_amount == 0 ? $invline ->remaining_amount + $invline->issued_amount : $invline ->previous_amount + $invline->remaining_amount)}}</td>
                                            <td class="text-right globalmargin" id="qty_{{$invline->idInvoiceLines}}">{{$invline->remaining_amount}}</td>
                                            <td class="text-center">
                                                @if($invline->remaining_amount != 0)
                                                        <a href="#" id="{{$invline->idInvoiceLines}}" onclick="issueItem({{ $invline->remaining_amount}},{{json_encode($invline)}})" data-toggle="modal" data-target="#serialModal">
                                                             Add
                                                        </a>
                                                    @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tfoot>
                                    <tr>
                                        <th>NO :</th>
                                        <th>Item Code</th>
                                        <th>Description</th>
                                        <th class="text-center">Ordered Qty</th>
                                        <th class="text-right">Remaining Qty</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div id="dnote1" class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center">Delivery Note</h3>
                            <hr />
                            <div class="row">
                                <div class="col-md-12">
                                    <table style="width: 100%" >
                                        <thead>
                                        <tr>
                                            <th class="pull-left">
                                                <h4>INV NO: {{$details->InvNumber}}</h4>
                                            </th>
                                            <th >
                                                <h5 class="pull-right">Dnote No : {{$dnote}}</h5>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th><h4 class="pull-left">Client Name: {{ucwords($details->clientAccount)}}</h4></th>
                                            <th ><h5 class="pull-right">Date: {{\Carbon\Carbon::now()->format(\Serial\Helper::helper()->dateFormat())}}</h5></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                            <hr />
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover table-responsive modalTable" border="1" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>NO :</th>
                                    <th>Description</th>
                                    <th>Qty</th>
                                </tr>
                                </thead>
                                <tbody class="modalTableRow">
                                @if($details->status == \App\Issue::FULLY_ISSUED || $details->status == \App\Issue::PARTIALLY_DELIVERED || $details->status == \App\Issue::PARTIALLY_ISSUED || $details->status == \App\Issue::NOT_ISSUED)
                                    <?php
                                    $numb = 1;
                                            foreach ($inlines as $inline)
                                                {
                                                    if(!$inline->previous_amount == 0 && ($inline->status == \App\Issuelines::ISSUED || $inline->status == \App\Issuelines::PARTIALLY_DELIVERED))
                                                        {
                                                            echo '<tr class="'.$inline->code.'"><td>'.$numb.'</td><td>'.ucwords($inline->cDescription).
                                                                    '</td><td>'.$inline->previous_amount.
                                                                    '</td></tr>';
                                                            $numb++;
                                                        }

                                                }
                                    ?>
                                 @endif
                                </tbody>
                            </table>
                        </div>
                        <div class="panel-footer" style="background-color: #ffffff">
                            <div class="row">
                            <div id="dnotefooter" class="col-md-12">
                            <table style="width: 100%">
                                <tbody>
                                    <tr>
                                        <td>Received the above goods in good order and condition</td>
                                    </tr>
                                <tr>
                                    <td style="padding-left: 6px; padding-top: 20px;">Received by ...............................................................</td>
                                    <td class="text-right" style="padding-left: 6px; padding-top: 20px;">Sign ...........................</td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 6px; padding-top: 20px">Date .................. Time ..................</td>
                                    <td rowspan="2">Co. Rubber Stamp</td>
                                </tr>
                                    <td style="padding-left: 6px; padding-top: 20px">Company Tel:</td>
                                <tr>
                                </tr>
                                </tbody>
                            </table>
                            </div>
                                <div id="issuebutton" class="pull-right">
                                    <a class="btn btn-danger btn-sm btn-raised" href="{{route('issue.index')}}">
                                        <i class="fa fa-close fa-lg "></i>
                                        Close
                                    </a>
                                    {{--                                    <a href="{{url('issuednote',['autoindexid'=>])}}">--}}
                                    <button type="submit" onclick="printContent('dnote1','{{$details->autoindex_id}}')" class="btn btn-success btn-sm btn-raised">
                                        <i class="fa fa-check fa-lg "></i> Issue </button>
                                </div>
                            </div>
                            </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    {{--<div><pre id="showerror"></pre></div>--}}

    <!-- Modal -->
    <div class="modal fade" id="serialModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="col-md-8" style="float: none; margin: 0 auto; margin-top: 110px" role="document">
            <div class="modal-content">
                <div class="modal-header btn-primary mainThemeColor">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center" id="myModalLabel">Enter Serial</h4>
                    <div class="row">
                        <div class="col-md-6">
                            <h4 id="itemDesc"></h4>
                        </div>
                        <div class="col-md-6">
                            <h4 id="itemCode"></h4>
                        </div>
                    </div>
                </div>
                    <div class="modal-body">

                        {{--{{ method_field('PUT') }}--}}
                <form onsubmit="return eventListner()" id="allinputs" method="POST" action="{{route('issue.store')}}">
                    <input id="token" type="hidden" name="_token" value="{{csrf_token()}}" />
{{--                    {{ csrf_field() }}--}}
                        <div class="col-md-12">
                            <div class="input-group">
                                    <input required class="form-control" id="search" type="text" value="">
                                <span class="input-group-btn"><button type="submit" class="btn btn-primary btn-sm btn-raised">
                                        <i class="fa fa-plus fa-lg "></i> Add </button>
                </form>
                                <button style="margin-left: 2px" type="button" id="dismiss" class="btn btn-sm btn-danger btn-raised" data-dismiss="modal">
                                    <i class="fa fa-close fa-lg "></i>Close</button></span>
                                </div>
                        </div>
                        <hr />
                        <input type='hidden' id='wholeData' name='wholeData'/>
                        <div class="modal-footer">

                            {{--<button type="submit" class="btn btn-warning btn-raised">Close</button>--}}
                        </div>

            </div>
        </div>
    </div>
    </div>
@endsection

@section('script')
    <script>
        var numb = 1;
        var remainingqty = 0;
        var iTemidInvoiceLines = '';
        var itemCode = '';
        var item_description = '';
        var dnote_setting = '{{\App\Setting::where('setting_id',3)->first()->default_value}}'
        var itemname = '';
        var theTotalItem = 0;

        function printContent(el,autoindexid){
            if($(".modalTableRow tr").length) {
                var dataTosend = {'id': autoindexid};
                $.ajax({
                    url: '{{url('issuednote')}}',
                    type: 'get',
                    data: dataTosend,
                    success: function (response) {
                        var restorepage = document.body.innerHTML;
                        var printcontent = document.getElementById(el).innerHTML;
                        document.body.innerHTML = printcontent;
                        window.print();
                        document.body.innerHTML = restorepage;
                        $(".modalTableRow").empty();
                        successMessage(response);
//                        console.log(reponse);
                        location.reload();
                    },
                    error: function (response) {
//                        console.log(response);
                    }
                });

            }
            else{
                errorMessaage('Delivery note is empty');
            }
        }

        function eventListner() {
            sendForm();
            return false
        };


        function issueItem(itemAmount,itemDetails) {
            $('#search').val('');
            theTotalItem = itemAmount;
            itemCode = itemDetails.code;
            item_description = itemDetails.cDescription;
//            console.log(itemDetails);
            remainingqty = parseInt($('#total_'+itemDetails.idInvoiceLines).html())
            iTemidInvoiceLines = itemDetails.idInvoiceLines;
            $("#wholeData").val(JSON.stringify(itemDetails));
            $(".warrant").select2();
            $('#itemCode').text('Item Code : ' + itemDetails.code);
            $('#itemDesc').text('Item Description : ' + itemDetails.cDescription);
        }


        function addRow(descr,code,idInvoiceLines)  {
            if($(".modalTableRow tr").hasClass(code) && dnote_setting == 1)
                {
                    $(".modalTableRow ." + code + " td:nth-child(3)").html(parseInt($(".modalTableRow ." + code + " td:nth-child(3)").html()) + 1);
                    $("#qty_"+iTemidInvoiceLines).html(parseInt($("#qty_"+iTemidInvoiceLines).html())-1);
                    if(parseInt($("#qty_"+iTemidInvoiceLines).html()) <= 0)
                    {
                        $("#"+iTemidInvoiceLines).remove();
                        closemodal();
                    }
                }
            else
                {
                    var itemd = dnote_setting == 1 ? numb : $('#search').val();
                        data = "<tr class='" + code + "'>" +
                                "<td>" + itemd + "</td>" +
                                "<td class='text-capitalize'>" + descr + "</td>" +
                                "<td> 1 </td>" +
                                "</tr>";

                $(".modalTableRow").append(data);
                $('#qty_'+iTemidInvoiceLines).html(parseInt($('#qty_'+iTemidInvoiceLines).html())-1);
                if(parseInt($("#qty_"+iTemidInvoiceLines).html()) <= 0)
                    {
                        $("#"+iTemidInvoiceLines).remove();
                    }
                numb++;
            }
        }

        function sendForm()
        {
            if($(".modalTableRow tr").hasClass(itemCode) && parseInt($(".modalTableRow ."+itemCode+" td:nth-child(3)").html()) >= remainingqty)
                {
                    errorMessaage('You can not issue more than Ordered qty');
                    $('#'+iTemidInvoiceLines).hide();
                    closemodal();
                }
            else {
                var formData = {};
                formData['serial'] = $('#search').val();
                formData['code'] = itemCode;
                formData['idInvoiceLines'] = iTemidInvoiceLines;
                formData['_token'] = $('#token').val();

                $.ajax({
                    url: '{{ route('issue.store') }}',
                    type: 'POST',
                    data: formData,
                    success: function (response) {
//                        console.log(response);
                        if (response == 'Serial not found') {
                            errorMessaage(response);
                            $('#search').val('');
                        }

                        else if (response == 'Invalid' || response == 'Issued' || response == 'Item code does not match serial code') {
                            errorMessaage(response + ' serial');
                            $('#search').val('');
                        }

                        else {
                            successMessage()
                            addRow(response.cDescription, response.code, response.idInvoiceLines);
//                            closemodal()
                            $('#search').val('');
                        }
                    },
                    error: function (response) {
//                        console.log(response);
                    }
                });
            }
        }


        function errorMessaage(msg) {
            sflash({
                title: "Error",
                text: msg,
                type: "error",
                allowOutsideClick: true,
                confirmButtonText: "Got it",
                showConfirmButton: true
            });
        }

        function successMessage() {
            sflash({
                title: "Activation",
                text: "Serials Activated successfully",
                type: "success",
                allowOutsideClick: true,
                showConfirmButton: false,
                timer: 900
            });
        }


    </script>
@endsection
