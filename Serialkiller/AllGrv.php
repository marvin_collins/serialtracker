<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 9/9/16
 * Time: 4:51 PM
 */

namespace Serial;
use App\Grv;
use App\GrvSerialized;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Session;


class AllGrv
{
    /**
     * AllGrv constructor.
     */
    protected $allGrvs;

    public static  function allGrv()
    {
     $grvAll = new AllGrv;
        return $grvAll;

}

    public function getAllGrves()
    {
        $grv = collect(DB::select(DB::raw('select AutoIndex,InvNumber,GrvNumber,GrvID,Description,DeliveryDate,OrderNum,cAccountName 
from InvNum where (DocType = 5 or DocType = 2) 
and DocState = 4  and  DocFlag =2 and OrigDocID <> 0')));
        $alreadyReceivedGrv = Grv::select('autoindex_id')->get()->flatten()->toArray();

             $itemWeWant = [];

                    foreach ($grv as $grvKey => $grvValue)
                    {
                        if(!in_array($grvValue->AutoIndex,array_flatten($alreadyReceivedGrv))){
                            $itemWeWant [] = $grvValue;
                        }
                    }
                    if(empty(array_flatten($alreadyReceivedGrv))){
                        self::storeGrv($grv);
                        return 'Grvs imported successfully';
                    }

                    if(empty($itemWeWant)){
                        return  'stop';
                    }

        self::storeGrv(collect($itemWeWant));

        return  'Grvs imported successfully';
    }

    private function storeGrv($grv)
    {
        $insertGrvArray = $grv->map(function ($item,$key){
            $now = Carbon::now();
            self::storeGrvInlinesItems($item->AutoIndex);
            return [
                'sagegrv_id' => $item->GrvID,
                'autoindex_id' =>$item->AutoIndex,
                'InvNumber'=>$item->InvNumber,
                'GrvNumber'=>$item->GrvNumber,
                'Description'=>$item->Description,
                'DeliveryDate'=>$item->DeliveryDate,
                'OrderNum'=>$item->OrderNum,
                'cAccountName'=>$item->cAccountName,
                'status' => Grv::UNSERIALIZED_GRV,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        })->toArray();
        Grv::insert($insertGrvArray);
    }



    public function storeGrvInlinesItems($autoindex)
    {
//        dd($autoindex);
//        $allGrvInlines = collect(DB::select(
//            DB::raw("select iInvoiceID,Invdate,idInvoiceLines,GRvid,invnum.GrvNumber,OrderNum,cAccountName,cDescription,fUnitcost,code,ItemGroup,fQuantity,fQtyToProcess,fUnitPriceExcl
//            from _btblInvoiceLines
//            inner join InvNum on _btblInvoiceLines .iInvoiceID=invnum.AutoIndex
//            inner join StkItem on StkItem.StockLink=_btblInvoiceLines.iStockCodeID
//            where AutoIndex =".$autoindex)));
        $allgrvinlines = self::getInlinesDetails($autoindex);
        $insertGrvInlineItems = $allgrvinlines->map(function ($itemInlines,$key){
            $now = Carbon::now();
            return [
                'autoindex_id'=>$itemInlines->iInvoiceID,
                'grvlines_id' =>$itemInlines-> idInvoiceLines,
                'description'=>$itemInlines->cDescription,
                'qty_serialized' =>0 ,
                'code' => $itemInlines->code,
                'fQuantity'=>$itemInlines->fQuantity,
                'qty_remaining' => $itemInlines->fQuantity,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        })->toArray();
        GrvSerialized::insert($insertGrvInlineItems);
        return  true;
    }


    public function getInlinesDetails($autoindex)
    {
        $inlinesDetails = collect(DB::select(DB::raw("
        select iInvoiceID,Invdate,idInvoiceLines,GRvid,invnum.GrvNumber,OrderNum,
        cAccountName,cDescription,fUnitcost,code,ItemGroup,fQuantity,fQtyToProcess,
        fUnitPriceExcl from _btblInvoiceLines inner join InvNum 
        on _btblInvoiceLines .iInvoiceID=invnum.AutoIndex 
        inner join StkItem on StkItem.StockLink=_btblInvoiceLines.iStockCodeID 
        where AutoIndex =".$autoindex)));
        return $inlinesDetails;
    }

    public function getItemDetails($item_id){
        $line_details = collect(DB::select(DB::raw("
        select iInvoiceID,Invdate,idInvoiceLines,GRvid,invnum.GrvNumber,OrderNum,
        cAccountName,cDescription,fUnitcost,code,ItemGroup,fQuantity,fQtyToProcess,
        fUnitPriceExcl from _btblInvoiceLines inner join InvNum 
        on _btblInvoiceLines .iInvoiceID=invnum.AutoIndex 
        inner join StkItem on StkItem.StockLink=_btblInvoiceLines.iStockCodeID 
        where idInvoiceLines = ".$item_id)))->first();


        return $line_details;
    }

    public function getGrvDetails($autoindex)
    {
//        session(['autoindex_id'=>$autoindex]);
        return  DB::select(DB::raw("select OrderNum,GrvID,GrvNumber,Description,InvDate,OrderDate,DeliveryDate,cAccountName from InvNum where AutoIndex = ". $autoindex));
    }



}


//        $grvlines = collect(DB::select(DB::raw("select iInvoiceID,Invdate,idInvoiceLines,GRvid,invnum.GrvNumber,OrderNum,cAccountName,cDescription,fUnitcost,code,ItemGroup,fQuantity,fQtyToProcess,fUnitPriceExcl from _btblInvoiceLines inner join InvNum on _btblInvoiceLines .iInvoiceID=invnum.AutoIndex inner join StkItem on StkItem.StockLink=_btblInvoiceLines.iStockCodeID where DocType=5 and DocFlag=2 and DocState=1"))
//        )->take(10);


//gettinginlines for a given grv
