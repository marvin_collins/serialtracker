<?php
/**
 * Created by PhpStorm.
 * User: marvin
 * Date: 9/27/16
 * Time: 4:06 PM
 */

namespace Serial;

use App\Issuelines;
use Carbon\Carbon;
use DB;
use App\Issue;


class IssueSo
{
    public static function issueSo()
    {
        return new self();
    }

    public function getInvoice()
    {
       $saleOrderInvoice = collect(DB::select(DB::raw('
            select AutoIndex,InvNumber,GrvID,GrvNumber,Client.Name 
            as name,Description,InvDate,DeliveryDate from 
            InvNum inner join Client on Client.DCLink = InvNum.AccountID
             where DocType = 0 and DocState = 4')));

        $syncedSo = Issue::select('autoindex_id')->get()->flatten()->toArray();

                if(empty($syncedSo))
                {
                    foreach ($saleOrderInvoice as $key => $soValue)
                    {
                        if(!in_array($soValue->AutoIndex,array_flatten($syncedSo)))
                        {
                            $newSo [] = $soValue;
                        }
                    }
                    self::storeSo($saleOrderInvoice);
                    return 'Sale Order updated successfully';
                }

                $newSo = [];

                    foreach ($saleOrderInvoice as $key => $soValue)
                    {
                        if(!in_array($soValue->AutoIndex,array_flatten($syncedSo)))
                        {
                            $newSo [] = $soValue;
                        }
                    }

                    if(empty(array_flatten($newSo)))
                    {
                        return 'ok';
                    }

                self::storeSo(collect($newSo));

//        dd(collect($newSo),Issue::all(),$saleOrderInvoice,array_flatten($syncedSo ));

        return 'Sale Order updated successfully';
    }

    public function storeSo($insertSo)
    {
        $now = Carbon::now();
            $insertSoArray = $insertSo->map(function ($value,$key) use($now)
            {
                return [
                    'sagegrv_id' => 1,
                    'autoindex_id' =>$value->AutoIndex,
                    'InvNumber'=>$value->InvNumber,
                    'GrvNumber'=>$value->GrvID,
                    'Description'=>$value->Description,
                    'DeliveryDate'=>$value->DeliveryDate,
                    'clientAccount'=>$value->name,
                    'status' => Issue::NOT_ISSUED,
                    'created_at' => $now,
                    'updated_at' => $now,
                ];
            })->toArray();

            Issue::insert($insertSoArray);


                foreach ($insertSoArray as $inlineKey => $inlinesValue )
                {
                    self::storeINVlines($inlinesValue['autoindex_id']);
                }

//                dd(Issuelines::all());
    }

    public function storeINVlines($autoindex)
    {
        $now = Carbon::now();
        $inlineDetails = AllGrv::allGrv()->getInlinesDetails($autoindex);
            $inertInlines = $inlineDetails->map(function ($invvalues, $invkey) use($now,$autoindex)
            {
                return [
                    'grv_id' => $invvalues->GRvid,
                    'autoindex_id' => $autoindex,
                    'idInvoiceLines' => $invvalues->idInvoiceLines,
                    'cDescription' => $invvalues->cDescription,
                    'fUnitcost' => $invvalues->fUnitcost,
                    'code' => $invvalues->code,
                    'fUnitPriceExcl' => $invvalues->fUnitPriceExcl,
                    'previous_amount'=>0,
                    'issued_amount' => 0,
                    'remaining_amount' => $invvalues->fQuantity,
                    'status' => Issuelines::PROCESSING,
                    'serial' => 0,
                    'created_at' => $now,
                    'updated_at' => $now
                ];
            })->toArray();

        Issuelines::insert($inertInlines);
//        dd($inertInlines,Issuelines::all());
    }

}